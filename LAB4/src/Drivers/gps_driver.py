#!/usr/bin/env python

import rospy
import serial
import utm
from std_msgs.msg import String
from std_msgs.msg import Header
from gps_puck.msg import gps


def gpgga_to_degrees(x):
    deg = int(x/100)
    deg = deg+(x/100-deg)*10/6
    return deg


if __name__ == '__main__':
    SENSOR_NAME = "gps_sensor"
    rospy.init_node('gps_node', anonymous=True)
    serial_port = rospy.get_param('~port', '/dev/ttyUSB1')
    serial_baud = rospy.get_param('~baudrate', 4800)
    ser = serial.Serial(serial_port, serial_baud, timeout=5)

    pub = rospy.Publisher('gps_data', gps, queue_size=10)
    rospy.logdebug("Ready to publish Data") 
    try:
        while not rospy.is_shutdown():
            line = ser.readline()

            if line == '':
                rospy.logwarn("No input from serial")
            else:
                if line_list[0] == '$GPGGA':
                    gpgga_data = str(line).split(',')
                    print(gpgga_data)
                    e = -1 if gpgga_data[5] == 'W' else 1
                    n = -1 if gpgga_data[3] == 'S' else 1
                    utm_data = utm.from_latlon(n*gpgga_to_degrees(
                        float(gpgga_data[2])), e*gpgga_to_degrees(float(gpgga_data[4])))

                    gps_values = gps()

                    gps_values.header.stamp = rospy.Time.now()
                    gps_values.header.frame_id = 'gps'
                    gps_values.latitude = gpgga_to_degrees(
                        float(gpgga_data[2]))
                    gps_values.longitude = gpgga_to_degrees(
                        float(gpgga_data[4]))
                    gps_values.altitude = float(gpgga_data[9])
                    gps_values.utm_easting = utm_data[0]
                    gps_values.utm_northing = utm_data[1]
                    gps_values.zone = utm_data[2]
                    gps_values.letter = utm_data[3]

                    pub.publish(gps_values)

    except rospy.ROSInterruptException:
        ser.close()

    except serial.serialutil.SerialException:
        rospy.loginfo("Shutting down gps node...")
