#!/usr/bin/env python


import rospy
import serial
import roslib
from sensor_msgs.msg import Imu, MagneticField
from tf.transformations import quaternion_from_euler
import math
if __name__=='__main__':

	imu_data_pub = rospy.Publisher('/imu', Imu, queue_size=1)
	imu_data=Imu()
	mag_data_pub = rospy.Publisher('/mag', MagneticField, queue_size=1)
	mag_data= MagneticField()

	rospy.init_node("imu_driver", anonymous=True)
	rate = rospy.Rate(40)
	serial_port ='/dev/ttyUSB0'
	serial_baud = 115200
	port = serial.Serial(serial_port,serial_baud,timeout=3.0)
	i=0
	while not rospy.is_shutdown(): line = str(port.readline()); print(line);data=line[:-8].split(','); imu_data.header.stamp = rospy.Time.now(); imu_data.orientation.x = float(data[1]); imu_data.orientation.y = float(data[2]); imu_data.orientation.z = float(data[3]); imu_data.orientation.w = 0;imu_data.linear_acceleration.x = float(data[7]);imu_data.linear_acceleration.y = float(data[8]); imu_data.linear_acceleration.z = float(data[9]); mag_data.header.stamp = rospy.Time.now(); mag_data.magnetic_field.x = float(data[4]); mag_data.magnetic_field.y = float(data[5]); mag_data.magnetic_field.z = float(data[6]); imu_data.angular_velocity.x=float(data[10]); imu_data.angular_velocity.y=float(data[11]); imu_data.angular_velocity.z=float(data[12]); imu_data_pub.publish(imu_data); mag_data_pub.publish(mag_data)
